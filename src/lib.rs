use discord_message_builder::MessageBuilder;
use lavalink::decoder::DecodedTrack;
use std::fmt::{Display, Formatter, Result as FmtResult};

#[derive(Debug)]
pub struct CurrentlyPlaying {
    paused: bool,
    position: u64,
    track: Option<DecodedTrack>,
}

impl CurrentlyPlaying {
    pub fn is_playing(&self) -> bool {
        self.track.is_some() && !self.paused
    }
}

impl Display for CurrentlyPlaying {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        match self.track.as_ref() {
            Some(track) => {
                let status = if self.paused {
                    "Paused"
                } else {
                    "Currently Playing"
                };

                write!(f,
                    "{}: **{}** by **{}** `[{}/{}]`\n{}",
                    status,
                    track.title,
                    track.author,
                    track_length_readable(self.position as u64),
                    track_length_readable(track.length),
                    track.url.as_ref().unwrap_or(&"(no url)".to_owned()),
                )
            },
            None => write!(f, "No song is currently playing.")
        }
    }
}

pub fn format_track(track: DecodedTrack) -> String {
    let mut msg = MessageBuilder::new();
    msg = msg.push_bold_safe(track.title);
    msg.0.push_str(" by ");
    msg = msg.push_bold_safe(track.author);
    msg.0.push(' ');

    let length = track_length_readable(track.length as u64);
    msg = msg.push_mono_safe(format!("[{}]", length));

    msg.build()
}

pub fn track_length_readable(ms: u64) -> String {
    let mut seconds = ms / 1000;

    if seconds == 0 {
        return "0s".to_owned();
    }

    if seconds < 60 {
        return format!("{}s", seconds);
    }

    let mut minutes = seconds / 60;
    seconds %= 60;

    if minutes < 60 {
        return format!("{}m {}s", minutes, seconds);
    }

    let hours = minutes / 60;
    minutes %= 60;

    format!("{}h {}m {}s", hours, minutes, seconds)
}

#[cfg(test)]
mod tests {
    use crate::CurrentlyPlaying;
    use lavalink::decoder::DecodedTrack;

    fn currently_playing() -> CurrentlyPlaying {
        CurrentlyPlaying {
            paused: true,
            position: 6400,
            track: None,
        }
    }

    fn state_track() -> CurrentlyPlaying {
        let mut state = currently_playing();

        state.track = Some(DecodedTrack {
            author: "xKito Music".to_owned(),
            identifier: "zcn4-taGvlg".to_owned(),
            length: 184_000,
            source: "youtube".to_owned(),
            stream: false,
            title: "she - Prismatic".to_owned(),
            url: "https://www.youtube.com/watch?v=zcn4-taGvlg".to_owned().into(),
            version: 1,
        });

        state
    }

    #[test]
    fn test_is_playing() {
        assert!(!currently_playing().is_playing());
        assert!(!state_track().is_playing());
    }

    #[test]
    fn test_state_format() {
        assert_eq!(
            format!("{}", currently_playing()),
            "No song is currently playing.",
        );

        assert_eq!(
            format!("{}", state_track()),
            "Paused: **she - Prismatic** by **xKito Music** `[6s/3m 4s]`
https://www.youtube.com/watch?v=zcn4-taGvlg");
    }

    #[test]
    fn test_track_length_readable() {
        assert_eq!(super::track_length_readable(1000).to_string(), "1s");
        assert_eq!(super::track_length_readable(62_043).to_string(), "1m 2s");
        assert_eq!(
            super::track_length_readable(3_674_000).to_string(),
            "1h 1m 14s",
        );
    }
}
